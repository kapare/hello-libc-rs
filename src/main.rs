#[cfg(target_os = "linux")]
extern crate libc;

fn test_cmsg_space() {
        unsafe {
            for l in 0..10 {
                libc::CMSG_SPACE(l);
    		println!("libc space: {}", l);
            }
        }
}

fn test_prctl() {

    	use libc::{prctl, PR_SET_CHILD_SUBREAPER};
        unsafe {
	    prctl(PR_SET_CHILD_SUBREAPER, 1, 0, 0);
        }
}

fn main() {
    	println!("hello libc Rust!");
	test_cmsg_space();
        test_prctl();
}
