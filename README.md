# hello-libc-rs

This project is simply a test to call libc function in Rust.

## Rust setup

Install:

```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

Creates the `hello-libc-rs` project: 

```
cargo new -bin hello-libc-rs
```

Install vim:

```
git clone https://github.com/rust-lang/rust.vim ~/.vim/pack/plugins/start/rust.vim
```

Simply type vim from the project root then `:make run`, or make <cargo_option>, from vim 

References:

* https://www.rust-lang.org/tools/install
* https://github.com/rust-lang/rust.vim

## Rust libc

The Rust libc:

Raw FFI (Foreign Function Interface) bindings to platforms system libraries  

Look at `libc-test/test/cmsg.rs` for examples.

References:

* https://crates.io/crates/libc
* https://doc.rust-lang.org/nomicon/ffi.html
